<?php
    use App\Entity\Divida;
    $id_divida = isset($_GET['id_divida']) ? $_GET['id_divida'] : 0;
    $devedor_id = isset($_GET['devedor_id']) ? $_GET['devedor_id'] : 0;
    $objDivida = new Divida;
    if($id_divida > 0){
        $objDivida = $objDivida->getDivida($id_divida);
    }
?>
<form action="app/Controller/DividaController.php" method="POST" class="formDivida">
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <div class="modal-header">
                    <h3 class="modal-title" id="modal-title-default"><?php echo $id_divida == 0 ? "Nova Dívida" : "Atualização de Dívida" ?></h3>
                    <span type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </span>
                </div>
                <div class="modal-body">

                    <input type="hidden" name="id" value="<?php echo $id_divida > 0 ? $id_divida : 0 ?>" id="id_divida">

                    <input type="hidden" name="devedor_id" value="<?php echo $devedor_id > 0 ? $devedor_id : 0 ?>" id="devedor_id">
                    <input type="hidden" name="action" value="<?php echo $id_divida > 0 ? 'update' : 'insert' ?>">
                    
                    <div class="form-row">
                        
                        <div class="form-group col-md-12 col-12">
                            <label for="descricao">Descrição</label>
                            <input type="text" class="form-control" id="descricao" name="descricao" value="<?php echo $id_divida > 0 ? $objDivida->descricao : '' ?>">
                        </div>
                        
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6 col-12">
                            <label for="valor">Valor</label>
                            <input type="text" maxlength="14" class="form-control valor" id="valor" value="<?php echo $id_divida > 0 ? number_format($objDivida->valor,2,',','.') : '' ?>" name="valor">
                        </div>
                        <div class="form-group col-md-6 col-12">
                            <label for="data_vencimento">Data de Vencimento</label>
                            <input type="text" class="form-control date" id="data_vencimento" name="data_vencimento"  value="<?php echo $id_divida > 0 ? date('d/m/Y', strtotime($objDivida->data_vencimento)) : '' ?>">
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <div class="row text-center">
                        <div class="col-12">
                            <button type="submit" class="btn btn-danger"><i class="fa fa-plus"></i> Gravar</button>
                            <span type="button" class="btn btn-dark" id="btnFechaModal" data-dismiss="modal"><i class="fa fa-close" aria-hidden="true"></i> Fechar</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>