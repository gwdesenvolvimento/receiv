<?php
    require '../../vendor/autoload.php';
    use App\Entity\Devedor;
    use App\Entity\Alert;

    try {
        $id                 = isset($_POST['id']) ? $_POST['id'] : 0;
        $nome               = isset($_POST['nome']) ? $_POST['nome'] : '';
        $cpf_cnpj           = isset($_POST['cpf_cnpj']) ? $_POST['cpf_cnpj'] : '';
        $data_nascimento    = isset($_POST['data_nascimento']) ? $_POST['data_nascimento'] : '';
        $endereco           = isset($_POST['endereco']) ? $_POST['endereco'] : '';
        $acao               = isset($_POST['action']) ? $_POST['action'] : '';

        if ((($nome == "") || ($cpf_cnpj == "") || ($data_nascimento == "") || ($endereco == "")) && ($acao != "DELETE")){
            
            echo json_encode(Alert::ErrorAlertAllRequiredField());

        }elseif((strtoupper($acao) != "INSERT") && (strtoupper($acao) != "UPDATE") && (strtoupper($acao) != "DELETE") && (strtoupper($acao) != "SELECT")){

            echo json_encode(Alert::ErrorAlertActionNotAllowed());

        }elseif((strtoupper($acao) == "INSERT")|| (strtoupper($acao) == "UPDATE")){

            $arrData = explode("/",$data_nascimento);
            $data_nascimento = $arrData[2]."-".$arrData[1]."-".$arrData[0];

            $obDevedor = new Devedor;
            $obDevedor->nome            = $nome;
            $obDevedor->cpf_cnpj        = $cpf_cnpj;
            $obDevedor->data_nascimento = $data_nascimento;
            $obDevedor->endereco        = $endereco;

            if(strtoupper($acao) == "INSERT"){

                if($obDevedor->cadastrar()){
                    echo json_encode(Alert::SuccessAlertInsertDataBase());
                }else{
                    echo json_encode(Alert::ErrorAlertGeneral());
                }

            }elseif(strtoupper($acao) == "UPDATE"){
                $obDevedor->id =  $id;
                if($obDevedor->atualizar()){
                    echo json_encode(Alert::SuccessAlertUpdateDataBase());
                }else{
                    echo json_encode(Alert::ErrorAlertGeneral());
                }

            }
            
        }elseif((strtoupper($acao) == "DELETE")){
            $obDevedor = new Devedor;
            $obDevedor->id = $id;
            if($obDevedor->excluir()){
                echo json_encode(Alert::SuccessAlertDeleteDataBase());
            }else{
                echo json_encode(Alert::ErrorAlertGeneral());
            }
        }

    } catch (Exception $e) {

        echo json_encode(Alert::customAlert(true,$e->getMessage(),'error'));

    }

?>