<?php 
    use App\Entity\Devedor;
    $id         = isset($_GET['id']) ? $_GET['id'] : 0;
    $objDevedor = new Devedor();
    if($id > 0){
        $objDevedor = $objDevedor->getDevedor($id);
    }
?>
<div class="container-fluid">
    <div class="row m-5">

        <div class="col-md-12 col-12">

            <div class="card">
                <h5 class="card-header bg-dark text-light">
                    Devedor
                </h5>
                <div class="card-body">
                    <!--Form -->
                    <form action="app/Controller/DevedorController.php" method="POST" class="form">
                        <input type="hidden" name="id" value="<?php echo $id > 0 ? $id : 0 ?>" id="id_devedor">
                        <input type="hidden" name="action" value="<?php echo $id > 0 ? 'update' : 'insert' ?>">
                        <div class="form-row">
                            <div class="form-group col-md-8 col-12">
                                <label for="nome">Nome</label>
                                <input type="text" class="form-control" id="nome" name="nome" value="<?php echo $id > 0 ? $objDevedor->nome : '' ?>">
                            </div>
                            <div class="form-group col-md-4 col-12">
                                <label for="cpf_cnpj">CPF / CNPJ</label>
                                <input type="text" maxlength="14" class="form-control" id="cpf_cnpj" value="<?php echo $id > 0 ? $objDevedor->cpf_cnpj : '' ?>" name="cpf_cnpj" onkeypress="return event.charCode >= 48 && event.charCode <= 57">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-4 col-12">
                                <label for="data_nascimento">Data de Nascimento</label>
                                <input type="text" class="form-control date" id="data_nascimento" name="data_nascimento"  value="<?php echo $id > 0 ? date('d/m/Y', strtotime($objDevedor->data_nascimento)) : '' ?>">
                            </div>
                            <div class="form-group col-md-8 col-12">
                                <label for="endereco">Endereço</label>
                                <input type="text" class="form-control" id="endereco" name="endereco" placeholder="Endereço" value="<?php echo $id > 0 ? $objDevedor->endereco : '' ?>">
                            </div>
                        </div>

                        <div class="col-md-12 col-12 text-right mb-3 d-flex justify-content-between">
                            <a class="btn btn-dark back" href="?page=devedor/index">
                                <i class="fa fa-arrow-left" aria-hidden="true"></i>
                                Voltar
                            </a>
                            <button type="submit" class="btn btn-danger">
                                <i class="fa fa-plus"></i>
                                Gravar
                            </button>
                        </div>

                    </form>
                    <!--Form -->
                </div>

                
            </div>

        </div>

    </div>
</div>

<div id="divDividasDevedor"></div>