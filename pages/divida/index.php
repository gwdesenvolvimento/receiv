<?php 
    use App\Entity\Divida;
    $devedor_id = isset($_GET['devedor_id']) ? $_GET['devedor_id'] : 0;
    $where = "devedor_id = ".$devedor_id;
    $objDivida = new Divida();
    $lista      = $objDivida->getDividas($where);
?>
<div class="container-fluid ">
    <div class="row m-5 ">
        <div class="col-md-12 col-12">

            <div class="card">
                <h5 class="card-header bg-dark text-light">
                    Dívidas
                </h5>
                <div class="card-body">
                    
                    <div class="col-md-12 col-12 text-right mb-3">
                        <button type="button" class="btn btn-danger" onclick="AbrirDivida(0,<?php echo $devedor_id ?>)">
                            <i class="fa fa-plus"></i>
                            Inserir Dívida
                        </button>
                    </div>
                    
                    <div class="col-md-12 col-12">
                        <div class="table table-responsive">
                            <table class="table table-striped table-bordered">
                                <thead class="thead-dark text-light">
                                    <tr class="text-center">
                                        <th>Descrição</th>
                                        <th>Valor</th>
                                        <th>Vencimento</th>
                                        <th>Ações</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if(count($lista) > 0){ ?>
                                        <?php foreach($lista as $item){ ?>
                                            <tr class="text-center">
                                                <td><?php echo $item->descricao ?></td>
                                                <td>R$ <?php echo number_format($item->valor,2,',','.') ?></td>
                                                <td><?php echo date('d/m/Y', strtotime($item->data_vencimento)) ?></td>
                                                <td>
                                                    <button type="button" class='btn btn-sm btn-danger' title="Editar" onclick="AbrirDivida(<?php echo $item->id ?>,<?php echo $item->devedor_id ?>)">
                                                        <i class='fa fa-edit'></i>
                                                    </button>

                                                    <button type="button" class='btn btn-sm btn-dark' title="Remover" onclick="RemoveDivida(<?php echo $item->id ?>, 'app/Controller/DividaController.php')">
                                                        <i class='fa fa-trash'></i>
                                                    </button>

                                                    <?php if($item->pago == 0){ ?>
                                                        <button type="button" class='btn btn-sm btn-success' title="Marcar como Paga" onclick="AlteraStatus(<?php echo $item->id ?>, 'app/Controller/DividaController.php',1)">
                                                            <i class="fa fa-thumbs-up" aria-hidden="true"></i>
                                                        </button>
                                                    <?php }else{ ?>
                                                        <button type="button" class='btn btn-sm btn-warning' title="Marcar como ABERTO" onclick="AlteraStatus(<?php echo $item->id ?>, 'app/Controller/DividaController.php',0)">
                                                            <i class="fa fa-thumbs-down" aria-hidden="true"></i>
                                                        </button>
                                                    <?php } ?>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    <?php }else{ ?>
                                        <tr class="text-center">
                                            <td colspan="4">
                                                <strong>Não há Registros</strong>
                                            </td>
                                        </tr>

                                    <?php } ?>
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>
</div>