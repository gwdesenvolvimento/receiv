<?php 
namespace App\Entity;

class Alert{

    public static function customAlert($error,$msg,$type){
        return array(
            'error' => $error,
            'msg'   => $msg,
            'type'  => $type
        ); 
    }
    public static function SuccessAlertInsertDataBase(){
        return array(
            'error' => false,
            'msg'   => "Dados Cadastrados com Sucesso!",
            'type'  => "success"
        ); 
    }

    public static function SuccessAlertDeleteDataBase(){
        return array(
            'error' => false,
            'msg'   => "Dados Removidos com Sucesso!",
            'type'  => "success"
        ); 
    }

    public static function SuccessAlertUpdateDataBase(){
        return array(
            'error' => false,
            'msg'   => "Dados Atualizados com Sucesso!",
            'type'  => "success"
        ); 
    }

    public static function ErrorAlertActionNotAllowed(){
        return array(
            'error' => true,
            'msg'   => "Ação não localizada!",
            'type'  => "error"
        ); 
    }

    public static function ErrorAlertAllRequiredField(){
        return array(
            'error' => true,
            'msg'   => "Todos os Campos são obrigatórios",
            'type'  => "error"
        ); 
    }

    public static function ErrorAlertGeneral(){
        return array(
            'error' => true,
            'msg'   => "Ocorreu um erro ao realizar esta Ação",
            'type'  => "error"
        ); 
    }
}