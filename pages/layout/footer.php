    
    <div class="modal fade" id="modal-padrao" tabindex="-1" role="dialog" aria-labelledby="modal-default" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content modal-lg" id="modal-conteudo">
            </div>
        </div>
    </div>

    <script src="includes/js/jquery-3.5.1.min.js"></script>
    <script type="text/javascript" src="includes/js/moment.js"></script>
    <script type="text/javascript" src="includes/js/daterangepicker.js"></script>
    <script src="includes/js/daterangepicker-pt_br.js"></script>
    <script src="includes/js/alert.js"></script>
    <script src="includes/js/maskMoney.js"></script>
    <script src="includes/js/app.js"></script>
    <script src="includes/js/bootstrap.min.js"></script>
  </body>
</html>