<?php 
    use App\Entity\Devedor;
    $objDevedor = new Devedor();
    $lista      = $objDevedor->getDevedores();
?>
<div class="container-fluid">
    <div class="row m-5">

        <div class="col-md-12 col-12 text-right mb-3">
            <a class="btn btn-danger" href="?page=devedor/adicionar&id=0">
                <i class="fa fa-plus"></i>
                Inserir
            </a>
        </div>
        
        <div class="col-md-12 col-12">
            <div class="table table-responsive">
                <table class="table table-striped table-bordered">
                    <thead class="thead-dark text-light">
                        <tr class="text-center">
                            <th>Nome</th>
                            <th>CPF / CNPJ</th>
                            <th>Data de Nascimento</th>
                            <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if(count($lista) > 0){ ?>
                            <?php foreach($lista as $item){ ?>
                                <tr class="text-center">
                                    <td><?php echo $item->nome ?></td>
                                    <td><?php echo $item->cpf_cnpj ?></td>
                                    <td><?php echo date('d/m/Y', strtotime($item->data_nascimento)) ?></td>
                                    <td>
                                        <a href='?page=devedor/adicionar&id=<?php echo $item->id?>' class='btn btn-sm btn-danger' title="Editar">
                                            <i class='fa fa-edit'></i>
                                        </a>

                                        <button class='btn btn-sm btn-dark' title="Remover" onclick="Remove(<?php echo $item->id ?>, 'app/Controller/DevedorController.php')">
                                            <i class='fa fa-trash'></i>
                                        </button>
                                    </td>
                                </tr>
                            <?php } ?>
                        <?php }else{ ?>
                            <tr class="text-center">
                                <td colspan="4">
                                    <strong>Não há Registros</strong>
                                </td>
                            </tr>

                        <?php } ?>
                        
                    </tbody>
                </table>
            </div>
        </div>

    </div>
</div>