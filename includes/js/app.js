$(document).ready(function(){
    $('.date').daterangepicker({
        locale:config,
        singleDatePicker: true,
        /*showDropdowns: true,
        minYear: 1901,
        maxYear: parseInt(moment().format('YYYY'),10)*/
    });

    $(".form").submit(function(e) {

        e.preventDefault(); 
        var form = $(this);
        var url  = form.attr('action');
        var type = form.attr('method');
        $.ajax({
            type: type,
            url: url,
            data: form.serialize(), 
            success: function(data){
                data = JSON.parse(data);


                swal(data.msg, {
                    icon: data.type,
                }).then((ok) => {
                    if(!data.error){
                        var volta = $('.back').attr('href');
                        location.href = volta;
                        
                    }
                });
                
            },
            error: function(data){
                data = JSON.parse(data);
                swal(data.msg, {
                    icon: data.type,
                }).then((ok) => {
                    
                });
            }
        });
    
    
    });

    CarregaDividasDevedor($('#id_devedor').val());
    
});
function CarregaDividasDevedor(id){
    if(id > 0){
        $('#divDividasDevedor').load('?page=divida/index&tipo=modulo&devedor_id='+id);
    } 
}
function navigate(url){
    location.href = url;
}
function Remove(id, url){
    swal({
        title: "Você tem Certeza?",
        text: "Ao confirmar esse processo não poderá mais ser revertido!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
    .then((willDelete) => {

        if (willDelete) {
            $.ajax({
                type: 'POST',
                url: url,
                data:{
                    id: id,
                    action:'DELETE'
                },
                cache: false,
                async: false,

                success: function (data) {
                    data = JSON.parse(data);
                    swal(data.msg, {
                        icon: data.type,
                    }).then((ok) => {
                        location.reload();
                    });
                },
                fail: function () {}
            });
        } else {
        swal("Ok! Não faremos nada!");
        }
    });
}
function RemoveDivida(id, url){
    swal({
        title: "Você tem Certeza?",
        text: "Ao confirmar esse processo não poderá mais ser revertido!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
    .then((willDelete) => {

        if (willDelete) {
            $.ajax({
                type: 'POST',
                url: url,
                data:{
                    id: id,
                    action:'DELETE'
                },
                cache: false,
                async: false,

                success: function (data) {
                    data = JSON.parse(data);
                    swal(data.msg, {
                        icon: data.type,
                    }).then((ok) => {
                        CarregaDividasDevedor($('#id_devedor').val()); 
                    });
                },
                fail: function () {}
            });
        } else {
        swal("Ok! Não faremos nada!");
        }
    });
}
function AlteraStatus(id, url,status){
    var msg = "";
    if(status == 0){
        msg = "Deseja marcar esta Dívida como ABERTA?";
    }else{
        msg = "Deseja marcar esta Dívida como PAGA?";
    }
    swal({
        title: "Você tem Certeza?",
        text: msg,
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
    .then((willDelete) => {

        if (willDelete) {
            $.ajax({
                type: 'POST',
                url: url,
                data:{
                    id: id,
                    pago:status,
                    action:'STATUS'
                },
                cache: false,
                async: false,

                success: function (data) {
                    data = JSON.parse(data);
                    swal(data.msg, {
                        icon: data.type,
                    }).then((ok) => {
                        CarregaDividasDevedor($('#id_devedor').val()); 
                    });
                },
                fail: function () {}
            });
        } else {
            wal("Ok! Não faremos nada!");
        }
    });
}
function AbrirDivida(id,devedor_id){
    $('#modal-conteudo').load("?page=divida/adicionar&tipo=modulo&devedor_id="+devedor_id+"&id_divida="+id, function () {
        $(".valor").maskMoney({decimal:",", thousands:"."});
        $('.date').daterangepicker({
            locale:config,
            singleDatePicker: true,
            showDropdowns: true,
            minYear: 1901,
            maxYear: parseInt(moment().format('YYYY'),10)
        });
        $(".formDivida").submit(function(e) {

            e.preventDefault(); 
            var form = $(this);
            var url  = form.attr('action');
            var type = form.attr('method');
            $.ajax({
                type: type,
                url: url,
                data: form.serialize(), 
                success: function(data){
                    data = JSON.parse(data);
    
                    swal(data.msg, {
                        icon: data.type,
                    }).then((ok) => {
                        if(!data.error){
                            CarregaDividasDevedor($('#devedor_id').val());
                            $('#btnFechaModal').trigger('click');
                        }
                    });
                    
                },
            });
        
        
        });
        $('#modal-padrao').modal({
            backdrop: 'static',
            keyboard: true
        }, 'show');
    });
}