<?php
    require '../../vendor/autoload.php';
    use App\Entity\Divida;
    use App\Entity\Alert;

    try {
        
        $id                 = isset($_POST['id']) ? $_POST['id'] : 0;
        $devedor_id         = isset($_POST['devedor_id']) ? $_POST['devedor_id'] : 0;
        $descricao          = isset($_POST['descricao']) ? $_POST['descricao'] : '';
        $valor              = isset($_POST['valor']) ? $_POST['valor'] : '';
        $data_vencimento    = isset($_POST['data_vencimento']) ? $_POST['data_vencimento'] : '';
        $acao               = isset($_POST['action']) ? $_POST['action'] : '';
        $pago               = isset($_POST['pago']) ? $_POST['pago'] : 0;
        

        if ((($devedor_id == "") || ($descricao == "") || ($valor == "") || ($data_vencimento == "")) && ($acao != "DELETE") && ($acao != "STATUS")){
            
            echo json_encode(Alert::ErrorAlertAllRequiredField());

        }elseif((strtoupper($acao) != "INSERT") && (strtoupper($acao) != "UPDATE") && (strtoupper($acao) != "DELETE") && (strtoupper($acao) != "STATUS")){

            echo json_encode(Alert::ErrorAlertActionNotAllowed());

        }elseif((strtoupper($acao) == "INSERT") || (strtoupper($acao) == "UPDATE")){
            $arrVenc = explode("/",$data_vencimento);
            $data_vencimento = $arrVenc[2]."-".$arrVenc[1]."-".$arrVenc[0];
            $obDivida = new Divida;
            $obDivida->devedor_id       = $devedor_id;
            $obDivida->descricao        = $descricao;
            $obDivida->data_vencimento  = $data_vencimento;
            $obDivida->valor            = $valor;

            if(strtoupper($acao) == "INSERT"){

                if($obDivida->cadastrar()){
                    echo json_encode(Alert::SuccessAlertInsertDataBase());
                }else{
                    echo json_encode(Alert::ErrorAlertGeneral());
                }

            }elseif(strtoupper($acao) == "UPDATE"){
                $obDivida->id =  $id;

                if($obDivida->atualizar()){
                    echo json_encode(Alert::SuccessAlertUpdateDataBase());
                }else{
                    echo json_encode(Alert::ErrorAlertGeneral());
                }

            }
            
        }elseif((strtoupper($acao) == "DELETE")){
            $obDivida = new Divida;
            
            $obDivida->id = $id;
            if($obDivida->excluir()){
                echo json_encode(Alert::SuccessAlertDeleteDataBase());
            }else{
                echo json_encode(Alert::ErrorAlertGeneral());
            }
        }elseif((strtoupper($acao) == "STATUS")){
            $obDivida = new Divida;
            $obDivida->id   =  $id;
            $obDivida->pago =  $pago;

            if($obDivida->AlteraStatus()){
                echo json_encode(Alert::SuccessAlertUpdateDataBase());
            }else{
                echo json_encode(Alert::ErrorAlertGeneral());
            }

        }

    } catch (Exception $e) {

        echo json_encode(Alert::customAlert(true,$e->getMessage(),'error'));

    }

?>