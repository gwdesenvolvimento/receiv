<?php

namespace App\Entity;

use \App\Db\Database;
use \PDO;

class Devedor{

  public $id;
  public $nome;
  public $cpf_cnpj;
  public $data_nascimento;
  public $endereco;
  public $created_at;
  public $updated_at;
  
  /**
   * Método responsável por cadastrar um novo devedor no banco
   * @return boolean
   */
  public function cadastrar(){
    
    //INSERIR O DEVEDOR NO BANCO
    $obDatabase = new Database('devedores');
    $this->id = $obDatabase->insert([
                                      'nome'            => $this->nome,
                                      'cpf_cnpj'        => $this->cpf_cnpj,
                                      'data_nascimento' => $this->data_nascimento,
                                      'endereco'        => $this->endereco
                                    ]);

    //RETORNAR SUCESSO
    return true;
  }

  /**
   * Método responsável por atualizar o devedor no banco
   * @return boolean
   */
  public function atualizar(){
    return (new Database('devedores'))->update('id = '.$this->id,[
                                                              'nome'            => $this->nome,
                                                              'cpf_cnpj'        => $this->cpf_cnpj,
                                                              'data_nascimento' => $this->data_nascimento,
                                                              'endereco'        => $this->endereco
                                                              ]);
  }

  /**
   * Método responsável por excluir o devedor do banco
   * @return boolean
   */
  public function excluir(){
    return (new Database('devedores'))->delete('id = '.$this->id);
  }

  /**
   * Método responsável por obter os devedores do banco de dados
   * @param  string $where
   * @param  string $order
   * @param  string $limit
   * @return array
   */
  public static function getDevedores($where = null, $order = null, $limit = null){
    return (new Database('devedores'))->select($where,$order,$limit)
                                  ->fetchAll(PDO::FETCH_CLASS,self::class);
  }

  /**
   * Método responsável por buscar um devedor com base em seu ID
   * @param  integer $id
   * @return Devedor
   */
  public static function getDevedor($id){
    return (new Database('devedores'))->select('id = '.$id)
                                  ->fetchObject(self::class);
  }

}