<?php
require 'config.php';
require 'vendor/autoload.php';

use App\Entity\Devedor;
use App\Entity\Divida;

$page = isset($_GET['page']) ? $_GET['page'] : 'dashboard/index';
$tipo = isset($_GET['tipo']) ? $_GET['tipo'] : '';


if($page == 'dashboard/index'){

    $obDivida = new Divida;
    $obDevedor = new Devedor;
    $lista_devedores = $obDevedor->getDevedores();

    $soma_abertos = 0;
    $soma_recebidos = 0;
    
    $lista_dividas = $obDivida->getDividas();
    foreach($lista_dividas as $item){
        if($item->pago == 1){
            $soma_recebidos += $item->valor;
        }else{
            $soma_abertos += $item->valor;
        }
    }

    $lista_vencendo_hoje = $obDivida->getDividas("data_vencimento = '".date('Y-m-d')."'");
}

if($tipo == ""){
    
    include 'pages/layout/header.php';
    include 'pages/layout/menu.php';
}

include "pages/".$page.".php";

if($tipo == ""){
    include 'pages/layout/footer.php';
}
