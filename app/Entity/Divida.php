<?php

namespace App\Entity;

use \App\Db\Database;
use \PDO;

class Divida{

  public $id;
  public $devedor_id;
  public $descricao;
  public $valor;
  public $data_vencimento;
  public $pago;
  
  public $created_at;
  public $updated_at;
  

  private function ValorGravacao($valor){
    $valor = str_replace(".", "", $valor);
    $valor = str_replace(",", ".", $valor);
    return $valor;
  }

  /**
   * Método responsável por cadastrar uma nova Divida no banco
   * @return boolean
   */
  public function cadastrar(){

    //INSERIR A DIVIDA NO BANCO
    $obDatabase = new Database('dividas');
    $this->id = $obDatabase->insert([
                                      'devedor_id'            => $this->devedor_id,
                                      'descricao'             => $this->descricao,
                                      'valor'                 => $this->ValorGravacao($this->valor),
                                      'data_vencimento'       => $this->data_vencimento
                                    ]);

    //RETORNAR SUCESSO
    return true;
  }

  /**
   * Método responsável por atualizar a divida no banco
   * @return boolean
   */
  public function atualizar(){
    return (new Database('dividas'))->update('id = '.$this->id,[
                                                                'descricao'             => $this->descricao,
                                                                'valor'                 => $this->ValorGravacao($this->valor),
                                                                'data_vencimento'       => $this->data_vencimento
                                                              ]);
  }

  /**
   * Método responsável por excluir a divida do banco
   * @return boolean
   */
  public function excluir(){
    return (new Database('dividas'))->delete('id = '.$this->id);
  }

  /**
   * Método responsável por obter as dividas do banco de dados
   * @param  string $where
   * @param  string $order
   * @param  string $limit
   * @return array
   */
  public static function getDividas($where = null, $order = null, $limit = null){
    return (new Database('dividas'))->select($where,$order,$limit)
                                  ->fetchAll(PDO::FETCH_CLASS,self::class);
  }

  /**
   * Método responsável por buscar uma divida com base em seu ID
   * @param  integer $id
   * @return Divida
   */
  public static function getDivida($id){
    return (new Database('dividas'))->select('id = '.$id)
                                  ->fetchObject(self::class);
  }


  public function AlteraStatus(){
    return (new Database('dividas'))->update('id = '.$this->id,['pago'  => $this->pago]);
  }

}