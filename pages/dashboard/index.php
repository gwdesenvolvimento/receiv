<div class="container-fluid ">
    <div class="row">

        <div class="col-12 col-md-3 text-center hlink" onclick="navigate('?page=devedor/index')">
            <div class="card border-dark mb-3 mt-5" >
                <div class="card-header bg-dark text-danger">
                    
                    <i class="fa fa-users align-middle fa-2x" aria-hidden="true"></i>
                    <span class="text-light align-middle ml-3 title-card"><strong>Devedores</strong></span>

                </div>
                <div class="card-body text-dark text-center">
                    <h1 class="card-title"><?php echo count($lista_devedores)?></h1>
                    <p class="card-text"><strong>Devedores Cadastrados</strong></p>
                </div>
            </div>
        </div>
        
        <div class="col-12 col-md-3 text-center">
            <div class="card border-dark mb-3 mt-5" >
                <div class="card-header bg-dark text-danger">
                    
                    <i class="fa fa-usd fa-2x align-middle" aria-hidden="true"></i>
                    <span class="text-light align-middle ml-3 title-card"><strong>Dívidas</strong></span>

                </div>
                <div class="card-body text-dark text-center">
                    <h1 class="card-title">R$ <?php echo number_format($soma_abertos,2,',','.')?></h1>
                    <p class="card-text"><strong>Total em Aberto</strong></p>
                </div>
            </div>
        </div>

        <div class="col-12 col-md-3 text-center">
            <div class="card border-dark mb-3 mt-5" >
                <div class="card-header bg-dark text-danger">
                    
                    <i class="fa fa-money fa-2x align-middle" aria-hidden="true"></i>
                    <span class="text-light align-middle ml-3 title-card"><strong>Receita</strong></span>

                </div>
                <div class="card-body text-dark text-center">
                    <h1 class="card-title">R$ <?php echo number_format($soma_recebidos,2,',','.') ?></h1>
                    <p class="card-text"><strong>Total Recebido</strong></p>
                </div>
            </div>
        </div>

        <div class="col-12 col-md-3 text-center">
            <div class="card border-dark mb-3 mt-5" >
                <div class="card-header bg-dark text-danger">
                    
                    <i class="fa fa fa-calendar-check-o fa-2x align-middle" aria-hidden="true"></i>
                    <span class="text-light align-middle ml-3 title-card"><strong>Diário</strong></span>

                </div>
                <div class="card-body text-dark text-center">
                    <h1 class="card-title"><?php echo count($lista_vencendo_hoje) ?></h1>
                    <p class="card-text"><strong>Vencem Hoje</strong></p>
                </div>
            </div>
        </div>

    </div>
</div>